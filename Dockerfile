FROM node:latest
WORKDIR /server
ADD ./code/server /server
RUN npm install
EXPOSE 3000
CMD npm start